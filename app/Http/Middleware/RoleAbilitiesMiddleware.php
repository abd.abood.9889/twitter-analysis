<?php

namespace App\Http\Middleware;

use App\Constants;
use Closure;
use Illuminate\Http\Request;

class RoleAbilitiesMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next, $role = null)
    {
        if ($role) {
            if ($role == Constants::Admin && auth()->user()->role_id != Constants::Admin) {
                abort(403);
            }
            if (!in_array(auth()->user()->role_id, [$role, Constants::Admin])) {
                abort(403);
            }
        }
        return $next($request);
    }
}
