<?php

namespace App\Http\Controllers;

use App\Constants;
use App\Models\Feedback;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Storage;

class AuthController extends Controller
{
    public function index()
    {
        return view('auth.login');
    }

    public function login(Request $request)
    {
        $user = User::where('email', $request->email)->first();
        if (!$user)
            return redirect()->back()->withInput($request->all())->with('error', 'Your email is incorrect');

        $credentials = [
            'email' => $user->email,
            'password' => $request->password,
        ];

        if (!Auth::attempt($credentials)) {

            return redirect()->back()->withInput($request->all())->with('error', 'Your password is incorrect');
        }

        switch ($user->role_id) {
            case Constants::Admin:
                $feed = Session::get('feed');
                if ($feed) {
                    $result = Feedback::create([
                        'user_id'=>Auth::id(),
                        'searche_id'=> $feed['searche_id'],
                        'feed'=> $feed['feed'],
                    ]);
                    Session::flash('feed');
                    return redirect()->route('searsh.show', $result->search->username)->with('success', 'Thanks For Your Feedback');
                    break;
                }
                else{
                return redirect()->route('admin.dashboard.index');
                }
                break;

            case Constants::User:
                $feed = Session::get('feed');
                if ($feed) {
                    $result = Feedback::create([
                        'user_id'=>Auth::id(),
                        'searche_id'=> $feed['searche_id'],
                        'feed'=> $feed['feed'],
                    ]);
                    Session::flash('feed');
                    return redirect()->route('searsh.show', $result->search->username)->with('success', 'Thanks For Your Feedback');
                    break;
                } else {
                    return redirect()->route('home');
                    break;
                }
        }
    }

    public function register(Request $request)
    {

        return view('auth.Register');
    }

    public function userRegister(Request $request)
    {
        $data = $request->validate([
            'email' => 'required|email|unique:users,email',
            'full_name' => 'required|min:2|max:255',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:6|max:255|confirmed',
        ]);

        $data['role_id'] = Constants::User;
        $data['password'] = Hash::make($request->password);
        $user = User::create($data);
         
        return $this->login($request);
    }

    public function logout()
    {

        Auth::logout();

        return redirect('/');
    }
}
