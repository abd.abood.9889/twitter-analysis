<?php

namespace App\Http\Controllers;

use App\Models\SiteInfo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function index(){
        $result = SiteInfo::get()->first();
        $user= Auth::user();
        return view('CMS.dashboard' , compact('user', 'result'));
    }

    
    
    public function updateApiKey(Request $request, $id){
        $data = $request->validate([
            'api_key' => 'required|min:6',
        ]);
        $result = SiteInfo::find($id); 
        $result->update([
            'api_key'=> $data['api_key']
        ]);
        return redirect()->route('admin.dashboard.index')->with('success', 'Updated Successfully');
    }
}
