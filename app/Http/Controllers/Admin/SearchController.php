<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\SearchRequest;
use App\Models\Search;
use App\Models\SiteInfo;
use App\Models\Tweets;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SearchController extends Controller
{

    public function index()
    {
        $searchs = Search::all();

        return view('CMS.Searchs.index', compact('searchs'));
    }

    public function search(SearchRequest $request)
    {
        $key = SiteInfo::get()->first();

        $api_url = 'https://www.page2api.com/api/v1/scrape';
        $payload = [
            "api_key" => $key->api_key,
            "url" => "https://twitter.com/" . $request->search,
            "parse" => [
                "name" => "[data-testid=UserName] span >> text",
                "verified" => "js >> document.querySelectorAll('[data-testid=UserName] svg[role=img]').length == 1",
                "description" => "[data-testid=UserDescription] >> text",
                "location" => "span[data-testid=UserLocation] >> text",
                "url" => "a[data-testid=UserUrl] >> text",
                "join_date" => "span[data-testid=UserJoinDate] >> text",
                "following" => "a[href*=following] > span > span >> text",
                "followers" => "a[href*=followers] > span > span >> text",
                "latest_tweets" => [
                    "0" => [
                        "_parent" => "[data-testid=tweet]",
                        "name" => "[data-testid='User-Names'] a >> text",
                        "user_url" => "[data-testid='User-Names'] a >> href",
                        "content" => "div[data-testid=tweetText] >> text",
                        "timestamp" => "time >> datetime",
                        "retweets" => "div[data-testid=retweet] >> text",
                        "likes" => "div[data-testid=like] >> text",
                        "replies" => "div[data-testid=reply] >> text"
                    ]
                ]
            ],
            "wait_for" => "[data-testid=tweet]",
            "premium_proxy" => "us",
            "real_browser" => true
        ];

        $postdata = json_encode($payload);
        $ch = curl_init($api_url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $data = json_decode($result, true);
        if (isset($data['error'])) {
            return redirect()->route('home')->with('success', 'Not Found');
        }
        return $this->store($data['result'], $request->search);
    }

    public function store($data, $username)
    {

        $search = Search::create([
            'user_id' => Auth::check() ? Auth::id() : null,
            'username' => $username,
            'name' => $data['name'],
            'is_verified' => $data['verified'],
            'description' => $data['description'],
            'location' => $data['location'],
            'url' => $data['url'],
            'join_date' => $data['join_date'],
            'following' => $data['following'],
            'followers' => $data['followers'],
        ]);
        $data['latest_tweets'];

        $latest_tweets = [];
        $i = 0;
        foreach ($data['latest_tweets'] as $item) {
            $latest_tweets[$i] = [
                'name' => $item['name'],
                'content' => $item['content'],
                'retweets' => $item['retweets'],
                'likes' => $item['likes'],
                'replies' => $item['replies'],
                'search_id' => $search->id,
            ];
            $i++;
        }
        Tweets::insert($latest_tweets);

        return redirect()->route('searsh.show', $username);
    }

    public function show($username)
    {
        $result = Search::with('tweets')->where('username', $username)->first();
        return view('CMS.Searchs.show', compact('result'));
    }

    public function destroy($id)
    {
        $result = Search::find($id);

        $result->delete();
        return 'Deleted Successfully';
    }
}
