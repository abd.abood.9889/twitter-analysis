<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Feedback;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class FeedbackController extends Controller
{

    public function index()
    {
        $feedbacks = Feedback::with('search')->get();

        return view('CMS.Feedbacks.index', compact('feedbacks'));
    }


    public function destroy($id)
    {
        $result = Feedback::find($id);

        $result->delete();
        return 'Deleted Successfully';
    }

    public function store(Request $request)
    {
        if (Auth::check()) {
            $feed = Session::get('feed');
            if ($feed)
                $date = $feed;

            else
                $date = $request->all();

            $date['user_id'] = Auth::id();
            $result = Feedback::create($date);
            return redirect()->route('searsh.show', $result->search->username)->with('success', 'Thanks For Your Feedback');
        } else {
            Session::put('feed', $request->all());
            $feed = Session::get('feed');
            return redirect()->route('login.index')->with('success','Please login to continue');
        }
    }
}
