<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{

    public function index()
    {
        $users = User::all();

        return view('CMS.Users.index', compact('users'));
    }


    public function show($id)
    {
        $result = User::find($id);
        return view('CMS.Users.show', compact('result'));
    }

    public function destroy($id)
    {
        $result = User::find($id);

        $result->delete();
        return 'Deleted Successfully';
    }
}
