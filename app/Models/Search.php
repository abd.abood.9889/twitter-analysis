<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Search extends Model
{
    use HasFactory; 

    protected $fillable = [
        'user_id',
        'username',
        'name',
        'join_date',
        'is_verified',
        'description',
        'location',
        'url',
        'following',
        'followers',
    ];

    public function tweets(){
        return $this->hasMany(Tweets::class);
    }
    
    public function feedbacks(){
        return $this->hasMany(Feedback::class,'searche_id');
    }

    public function user(){
        return $this->belongsTo(User::class);
    }
}
