<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tweets extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'content',
        'retweets',
        'likes',
        'replies',
        'searche_id',
    ];

}
