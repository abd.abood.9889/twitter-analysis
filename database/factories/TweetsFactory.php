<?php

namespace Database\Factories;

use App\Models\Search;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Tweets>
 */
class TweetsFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'search_id' => Search::all()->Random()->first()->id,
            'name' => $this->faker->name(),
            'content' => $this->faker->text(200),
            'retweets' => $this->faker->numberBetween(1,10000),
            'likes' => $this->faker->numberBetween(1,10000),
            'replies' => $this->faker->numberBetween(1,10000),
        ];
    }
}
