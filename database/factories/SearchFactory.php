<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Search>
 */
class SearchFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name(),
            'join_date' => $this->faker->date(),
            'username' => $this->faker->userName(),
            'is_verified' => $this->faker->boolean(),
            'description' => $this->faker->text(200),
            'location' => $this->faker->country(),
            'url' => $this->faker->url(),
            'following' => $this->faker->numberBetween(1,10000),
            'followers' => $this->faker->numberBetween(1,10000),
        ];
    }
}
