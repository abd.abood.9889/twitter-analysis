<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('searches', function (Blueprint $table) {
            $table->id(); 
            $table->foreignId('user_id')->nullable()->constrained('users')->cascadeOnDelete();
            $table->string('username');
            $table->string('name');
            $table->string('join_date')->nullable();
            $table->boolean('is_verified');
            $table->text('description')->nullable();
            $table->string('location')->nullable();
            $table->string('url')->nullable();
            $table->string('following')->nullable();
            $table->string('followers')->nullable(); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('searches');
    }
};
