<?php

namespace Database\Seeders;

use App\Models\Tweets;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class TweetsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Tweets::factory()
        ->count(50)
        ->create();
    }
}
