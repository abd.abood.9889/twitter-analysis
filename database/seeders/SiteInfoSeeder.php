<?php

namespace Database\Seeders;

use App\Models\SiteInfo;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class SiteInfoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $key = [
            'id' => 1,
            'api_key' => '295d0446da75bcf69d973e29434f12287eb94065',
        ];
       
        SiteInfo::create($key);
    }
}
