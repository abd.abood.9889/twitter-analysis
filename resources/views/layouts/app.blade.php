<!doctype html>

<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('layouts.includes.links')
    @yield('style')
</head>
<body>

    <div id="app">
        @include('layouts.includes.nave')
        @include('layouts.includes.errorMessage')
        <main>
            @yield('content')
        </main>
        @include('layouts.includes.scripts')
    </div>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    @if(session('success'))
    <script>
        swal("{{session('success')}}")
    </script>

    @endif

    @include('layouts.includes.footer') 
</body>

</html>
