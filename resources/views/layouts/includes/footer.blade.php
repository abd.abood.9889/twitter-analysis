<!-- Footer -->
<style>
   
</style>
<footer class="text-center text-lg-start bg-white text-muted">
  <section class="d-flex justify-content-center justify-content-lg-between p-4 border-bottom">

  </section>
 
  {{-- <section class="">
    <div class="container text-center text-md-start mt-5">
      <div class="row mt-3">
        <div class="col-md-3 col-lg-4 col-xl-3 mx-auto mb-4">
          <a href="" style="text-decoration: none; color:black;" >
          <h6 class="text-uppercase fw-bold mb-4">
            <i class="fas fa-gem me-3 text-secondary"></i>British Linguistics Board
          </h6>
            </a>
          <article>
            The BLB ignites a standard of excellence with an accreditation process based on the Baldrige Education Criteria for Performance Excellence.
          </article>
          <div style="margin-top: 15px; ">
            <a href="https://www.facebook.com/blborg1" class="me-4 link-secondary" style="text-decoration: none" >
              <i class="fab fa-facebook-f" style="color:rgb(45, 45, 227);"></i>
            </a>
            <a href="https://twitter.com/blborg1" class="me-4 link-secondary" style="text-decoration: none">
              <i class="fab fa-twitter" style="color:rgb(23, 165, 197);"></i>
            </a>
            
            <a href="https://www.instagram.com/blborg1/" class="me-4 link-secondary" style="text-decoration: none">
              <i class="fab fa-instagram" style="color:rgb(172, 35, 35);"></i>
            </a>
            
          </div>
        </div>
      
        <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mb-4">
          <article>
            <a href="#" class="text-reset">Contact Us</a>
          </article>
          <article>
            <a href="#" class="text-reset">Q&A</a>
          </article>
          <article>
            <a href="#" class="text-reset">Standards</a>
          </article>
          <article>
            <a href="#" class="text-reset">Institutional Members</a>
          </article>
        </div>
        
        <div class="col-md-3 col-lg-2 col-xl-2 mx-auto mb-4">
          
          <article>
            <a href="#" class="text-reset">Partner membership</a>
          </article>
          <article>
            <a href="#" class="text-reset">Qualifications</a>
          </article>
          <article>
            <a href="#" class="text-reset">MISSION</a>
          </article>
          <article>
            <a href="#" class="text-reset">Code Of Conduct</a>
          </article>
        </div>
        
        <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4">
          <h6 class="text-uppercase fw-bold mb-4">Contact</h6>
          <article><i class="fas fa-home me-3 text-secondary"></i> Piccadilly Business Centre, Aldow Enterprise Park, 
            Manchester City ,  M12 6AE, England , the United Kingdom</article>
          <article>
            <i class="fas fa-envelope me-3 text-secondary"></i>
            contact@blb21.org.uk
          </article>
         
        </div>
      </div>
    </div>
  </section> --}}
  
  <div class="text-center p-4" style="background-color: rgba(0, 0, 0, 0.025);">
    © {{ now()->year }} Copyright:
    <a class="text-reset fw-bold" href="/">Twitter Analysis</a>
  </div>
</footer>
