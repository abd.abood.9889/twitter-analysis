@extends('layouts.app')
@section('style')
<link rel="stylesheet" href="frontend/home.css">
@endsection
@section('content')

<div class="hero">
    <div class="container">
        <div class="formbold-main-wrapper">
            <div class="formbold-form-wrapper">
                <h1>People Search
                </h1>
                <p>
                    Search for someone by his username,
                    and get all information about this account
                </p>
              <form action="{{route('search')}}" method="POST">
                @csrf
                <div class="formbold-email-subscription-form">
                  <input
                    type="text"
                    name="search"
                    id="text"
                    placeholder="Username"
                    class="formbold-form-input"
                  />
                    
                  <button class="formbold-btn">
                    Search
                    <img src="search.jpg" alt="" style="height: 14px;width:14px;">
                  </button>
                </div>
          
              </form>
            </div>
        </div>
          
    
    </div>
</div>

@endsection
