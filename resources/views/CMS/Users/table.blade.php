@foreach ($users as $user)
    <tr>
        <td>{{ $user->id }}</td>
        <td>{{ $user->full_name}}</td>
        <td>{{ $user->email}}</td>
        <td>
            <div class="dropdown">
                <button type="button" class="btn p-0 dropdown-toggle hide-arrow" data-bs-toggle="dropdown">
                    <i class="bx bx-dots-vertical-rounded"></i>
                </button>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="{{ route('admin.users.show', $user->id) }}"><i
                            class="bx bx-edit-alt me-1"></i>
                        Show </a>
                    <a class="dropdown-item deleteItem" data-url='{{ route('admin.users.destroy', $user->id) }}'
                        href="#"><i class="bx bx-trash me-1"></i>
                        Delete</a>
                </div>
            </div>
        </td>
    </tr>
@endforeach
