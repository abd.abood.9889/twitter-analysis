@extends('CMS.layouts.app')
@section('content')
<div class="container-xxl flex-grow-1 container-p-y">
    <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">Users /</span> {{$result->full_name}}</h4>
    
       
        {{-- <p> Image </p>
        <img src="{{ $profile_image}}" alt="" style="width: 300px; height:300px; border-radius:50%"> --}}

        <div class="card-body demo-vertical-spacing demo-only-element">
            
            <div class="container-xxl flex-grow-1 container-p-y">
                <div class="card h-100">
                    <div class="row">
                        <div class="col-lg-12 col-12">
                            <div class="card-header">
                                <h5> Name : {{$result->full_name}}</h5>
                                <hr>
                            </div>

                            
                            <div class="card-body">
                              <h2>Searchs :</h2>
                              @foreach ($result->searches as $search)
                                  <p>Search Result : <br> <a href="{{route('searsh.show', $search->username)}}">{{$search->username}}</a> </p>
                              @endforeach
                            </div>

                            <div class="card-body mt-5">
                                <h2>Feedbacks :</h2>
                                @foreach ($result->feedbacks as $feedback)
                                <p>Search Result : <br> <a href="{{route('searsh.show', $feedback->search->username)}}"> {{$feedback->search->username}}</a> </p>
                                <p> Feedback : {{ $feedback->feed }} </p>
                                <br>
                                @endforeach
                            </div>
                           
                        </div>
                        
                        
                    </div>
                    
                </div>
            </div>
            
        </div>


        <hr class="my-5" />
    </div>
@endsection
