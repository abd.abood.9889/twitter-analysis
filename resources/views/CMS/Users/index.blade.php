@extends('CMS.layouts.app')
@section('content')
    <div class="container-xxl flex-grow-1 container-p-y">
        <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">Dashboard /</span> Users</h4>

        <!-- Basic Bootstrap Table -->

        <div class="card h-100">
            <div class="row">
                <div class="col-lg-10 col-12">
                    <h5 class="card-header">Users</h5>
                </div>
                <div class="col-lg-2 col-12 p-3">
                    {{-- <a href="{{ route('Searchs.create') }}" class="btn btn-primary btn-block w-100 ">
                        اضافة عضو
                    </a> --}}
                </div>
            </div>
            <div class="table-responsive text-nowrap h-100">

                <table class="table">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody class="table-border-bottom-1 ">
                        @include('CMS.Users.table')
                    </tbody>
                </table>
            </div>
        </div>
        <hr class="my-5" />
    </div>
@endsection
