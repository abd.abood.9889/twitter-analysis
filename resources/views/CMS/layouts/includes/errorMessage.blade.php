@if (!$errors->isEmpty())
    <section class="section alert alert-danger row w-100 mt-4" style="margin: 0 auto;">
        <div class=" col-12">
            @foreach ($errors->all() as $error)
                <div>
                    <span class="text-danger"
                        title="This is an error message with hyperlink but you can also do  this  b "
                        class="alert-message">
                        .
                        {{ $error }}
                    </span>
                </div>
            @endforeach


        </div>
        
    </section>
@endif
