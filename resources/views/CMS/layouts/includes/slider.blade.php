 <!-- Menu -->

 <aside id="layout-menu" class="layout-menu menu-vertical menu bg-menu-theme">
     <div class="app-brand demo">
         <a href="{{ route('admin.dashboard.index') }}" class="app-brand-link" style="margin:0 auto; ">
             <span class="app-brand-logo demo ">
                 <img src="{{ asset('logo.png') }}" width="100px" height="100px" alt="">
                
                 <p>
                    Twitter Analysis
                 </p>
                 
             </span>
         </a>

         <a href="javascript:void(0);" class="layout-menu-toggle menu-link text-large ms-auto d-block d-xl-none">
             <i class="bx bx-chevron-left bx-sm align-middle"></i>
         </a>
     </div>

     <div class="menu-inner-shadow"></div>

     <ul class="menu-inner py-1 mt-4">
        <!-- Dashboard -->
        <li class="menu-item {{ Route::is('admin.dashboard.index') ? 'active' : '' }}">
            <a href="{{ route('admin.dashboard.index') }}" class="menu-link">
                <i class="menu-icon tf-icons bx bx-home-circle"></i>
                <div data-i18n="Analytics">Dashboard</div>
            </a>
        </li>
        <li
            class="menu-item {{ Route::is('admin.users.index')  ? 'open' : '' }}">
            <a href="javascript:void(0);" class="menu-link menu-toggle">
                <i class="menu-icon tf-icons bx bx-layout"></i>
                <div data-i18n="Layouts">Users</div>
            </a>
            <ul class="menu-sub">
                <li class="menu-item {{ Route::is('admin.users.index') ? 'active' : '' }}">
                    <a href="{{ route('admin.users.index') }}"
                        class="menu-link {{ Route::is('admin.users.index') ? 'open' : '' }}">
                        <div data-i18n="Without menu">Users</div>
                    </a>
                </li>

            </ul>
        </li>
        <li
            class="menu-item {{ Route::is('admin.searchs.index') || Route::is('admin.feedbacks.index')  ? 'open' : '' }}">
            <a href="javascript:void(0);" class="menu-link menu-toggle">
                <i class="menu-icon tf-icons bx bx-layout"></i>
                <div data-i18n="Layouts">Searchs</div>
            </a>
            <ul class="menu-sub">
                <li class="menu-item {{ Route::is('admin.searchs.index') ? 'active' : '' }}">
                    <a href="{{ route('admin.searchs.index') }}"
                        class="menu-link {{ Route::is('admin.searchs.index') ? 'open' : '' }}">
                        <div data-i18n="Without menu">Search Logs</div>
                    </a>
                </li>
                <li class="menu-item {{ Route::is('admin.feedbacks.index') ? 'active' : '' }}">
                    <a href="{{ route('admin.feedbacks.index') }}"
                        class="menu-link {{ Route::is('admin.feedbacks.index') ? 'open' : '' }}">
                        <div data-i18n="Without menu">Feedbacks</div>
                    </a>
                </li>
            </ul>
        </li>
       

       
            
        {{-- <li
            class="menu-item {{ Route::is('accountSetting.index') ? 'open' : '' }}">
            <a href="javascript:void(0);" class="menu-link menu-toggle">
                <i class="menu-icon tf-icons bx bx-dock-top"></i>
                <div data-i18n="Account Settings">Account Settings</div>
            </a>
            <ul class="menu-sub">
                <li class="menu-item {{ Route::is('accountSetting.index') ? 'active' : '' }}">
                    <a href="{{ route('accountSetting.index') }}" class="menu-link">
                        <div data-i18n="Account">Account</div>
                    </a>
                </li>
                
            </ul>
        </li>  --}}
        
     </ul>
 </aside>
