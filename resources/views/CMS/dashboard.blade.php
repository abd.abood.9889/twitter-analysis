@extends('CMS.layouts.app')
@section('content')
    <div class="container-xxl flex-grow-1 container-p-y">
        <div class="row">
            <div class="col-lg-12 mb-4 order-0">
                <div class="card">
                    <div class="d-flex align-items-end row">
                        <div class="col-sm-7">
                            <div class="card-body">
                                <h5 class="card-title text-primary">Hello {{ auth()->user()->full_name }} 🎉</h5>
                                <p class="mb-4">
                                    Welcome to your dashboard
                                    <br>
                                    <a href="{{route('admin.searchs.index')}}"class="btn btn-lg btn-outline-primary mt-5">Check New Searshs</a>
                                </p>
                            </div>
                        </div>
                        <div class="col-sm-5 text-center text-sm-left">
                            <div class="card-body pb-0 px-0 px-md-4">
                                <img src="{{ asset('dashborad/assets/img/illustrations/man-with-laptop-light.png') }}"
                                    height="140" alt="View Badge User"
                                    data-app-dark-img="illustrations/man-with-laptop-dark.png"
                                    data-app-light-img="illustrations/man-with-laptop-light.png" />
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div> 
        
        <div class="row">
            <div class="col-lg-12 mb-4 order-0">
                <div class="card">
                    <div class="d-flex align-items-end row">
                        <div class="col-sm-7">
                            <div class="card-body">
                                <h5 class="card-title text-primary">Change Api Key</h5>
                                <form action="{{route('admin.apikey.update' ,$result->id)}}" method="post">
                                    @csrf
                                    @method('put')
                                    <div class="card-body demo-vertical-spacing demo-only-element">
                                    <div class="input-group">
                                        <img class="input-group-text" width="55px"
                                            src="{{asset('c52b4a8f7966629175770176be54e9c2.png')}}" />
                                        {{-- <span >Full name</span> --}}
                                        <input type="text" required
                                            class="form-control {{ $errors->has('api_key') ? ' is-invalid' : '' }}"
                                            placeholder="api_key" aria-label="api_key" value="{{$result->api_key}}"
                                            name='api_key' />
                                        @if ($errors->has('api_key')) 
                                            <span class="invalid-feedback text-right" style="display: block;" role="alert">
                                                <strong>{{ $errors->first('api_key') }}</strong>
                                            </span>
                                        @endif
                                        <button class="btn btn-primary btn-style " type="submit">Update</button>
                                    </div>
                                </form> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
        
    </div>
    
@endsection
