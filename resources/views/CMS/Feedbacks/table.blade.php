@foreach ($feedbacks as $feedback)
    <tr>
        <td>{{ $feedback->id }}</td>
        <td> <a href="{{ route('admin.users.show', $feedback->user->id) }}">{{ $feedback->user->full_name }}</a></td>
        <td>{{ $feedback->search->username}}</td>
        <td>{{ $feedback->feed }}</td>
        <td>
            <div class="dropdown">
                <button type="button" class="btn p-0 dropdown-toggle hide-arrow" data-bs-toggle="dropdown">
                    <i class="bx bx-dots-vertical-rounded"></i>
                </button>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="{{ route('searsh.show', $feedback->search->username) }}"><i
                            class="bx bx-edit-alt me-1"></i>
                        Show Search Log</a>
                    <a class="dropdown-item deleteItem" data-url='{{ route('admin.feedbacks.destroy', $feedback->id) }}'
                        href="#"><i class="bx bx-trash me-1"></i>
                        Delete</a>
                </div>
            </div>
        </td>
    </tr>
@endforeach
