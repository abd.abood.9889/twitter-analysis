@extends('CMS.layouts.app')
@section('content')
    <div class="container-xxl flex-grow-1 container-p-y">
        <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">Account Settings /</span> Account</h4>

        <div class="row">
            <div class="col-md-12">
                @include('CMS.layouts.includes.accountSetting')
                <div class="card mb-4">
                    <h5 class="card-header">Profile Details</h5>
                    <!-- Account -->
                    {{-- <div class="card-body">
                        <div class="d-flex align-items-start align-items-sm-center gap-4">
                            <img src="../assets/img/avatars/1.png" alt="user-avatar" class="d-block rounded" height="100"
                                width="100" id="uploadedAvatar" />
                            <div class="button-wrapper">
                                <label for="upload" class="btn btn-primary me-2 mb-4" tabindex="0">
                                    <span class="d-none d-sm-block">Upload new photo</span>
                                    <i class="bx bx-upload d-block d-sm-none"></i>
                                    <input type="file" id="upload" class="account-file-input" hidden
                                        accept="image/png, image/jpeg" />
                                </label>
                                <button type="button" class="btn btn-outline-secondary account-image-reset mb-4">
                                    <i class="bx bx-reset d-block d-sm-none"></i>
                                    <span class="d-none d-sm-block">Reset</span>
                                </button>

                                <p class="text-muted mb-0">Allowed JPG, GIF or PNG. Max size of 800K</p>
                            </div>
                        </div>
                    </div> --}}
                    <hr class="my-0" />
                    <div class="card-body">
                        <form id="formAccountSettings" action="{{ route('accountSetting.update') }}" method="post"
                            enctype="multipart/form-data">
                            @method('PUT')
                            @csrf
                            <div class="row">
                                <div class="mb-3 col-md-6">
                                    <label for="full_name" class="form-label">Full Name</label>
                                    <input class="form-control {{ $errors->has('full_name') ? ' is-invalid' : '' }}"
                                        type="text" id="full_name" name="full_name" value="{{ $user->full_name }}"
                                        autofocus placeholder="Full Name" />
                                    @if ($errors->has('full_name'))
                                        <span class="invalid-feedback text-right" style="display: block;" role="alert">
                                            <strong>{{ $errors->first('full_name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="mb-3 col-md-6">
                                    <label for="address" class="form-label">Address</label>
                                    <input type="text"
                                        class="form-control {{ $errors->has('address') ? ' is-invalid' : '' }}"
                                        id="address" name="address" placeholder="Address" value="{{ $user->address }}" />
                                    @if ($errors->has('address'))
                                        <span class="invalid-feedback text-right" style="display: block;" role="alert">
                                            <strong>{{ $errors->first('address') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="mb-3 col-md-6">
                                    <label for="email" class="form-label">E-mail</label>
                                    <input class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}"
                                        type="text" id="email" name="email" value="{{ $user->email }}"
                                        placeholder="john.doe@example.com" />
                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback text-right" style="display: block;" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="mb-3 col-md-6">
                                    <label for="city" class="form-label">City</label>
                                    <input type="text"
                                        class="form-control {{ $errors->has('city') ? ' is-invalid' : '' }}" id="city"
                                        name="city" value="{{ $user->city }}" />
                                    @if ($errors->has('city'))
                                        <span class="invalid-feedback text-right" style="display: block;" role="alert">
                                            <strong>{{ $errors->first('city') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="mb-3 col-md-6">
                                    <label class="form-label" for="phone_number">Phone Number</label>
                                    <div class="input-group input-group-merge">
                                        {{-- <span class="input-group-text">US (+1)</span> --}}
                                        <input type="text" id="phone_number" name="phone_number"
                                            class="form-control {{ $errors->has('phone_number') ? ' is-invalid' : '' }}"
                                            placeholder="202 555 0111" value="{{ $user->phone_number }}" />
                                        @if ($errors->has('phone_number'))
                                            <span class="invalid-feedback text-right" style="display: block;"
                                                role="alert">
                                                <strong>{{ $errors->first('phone_number') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="mb-3 col-md-6">
                                    <label class="form-label" for="country">Country</label>
                                    <input type="text"
                                        class="form-control {{ $errors->has('country') ? ' is-invalid' : '' }}"
                                        id="country" name="country" value="{{ $user->country }}" />
                                    @if ($errors->has('country'))
                                        <span class="invalid-feedback text-right" style="display: block;" role="alert">
                                            <strong>{{ $errors->first('country') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            @if (isStudent())
                                <div class="mb-3 col-md-12">
                                    <div>
                                        <label for="passport" class="form-label">Passport</label>
                                    </div>
                                    <img @if ($user->passport == null) src="{{ asset('upload.png') }}" @else src="{{ asset('storage/' . $user->passport) }}" @endif
                                        id="output" class="rounded my-2" alt="" width="150px" height="150px">
                                    <img src="{{ asset($user->passport) }}" alt="">
                                    <input
                                        onchange="document.getElementById('output').src = window.URL.createObjectURL(this.files[0])"
                                        type="file" class="form-control" id="passport" name="passport"
                                        placeholder="231465" maxlength="6" />
                                    @if ($errors->has('passport'))
                                        <span class="invalid-feedback text-right" style="display: block;" role="alert">
                                            <strong>{{ $errors->first('passport') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            @endif
                            <div class="mt-2">
                                <button type="submit" class="btn btn-primary btn-style-without-float me-2">Save
                                    changes</button>
                                {{-- <button type="reset"
                                    class="btn btn-outline-secondary btn-style-without-float">Cancel</button> --}}
                            </div>
                        </form>
                    </div>
                    <!-- /Account -->
                </div>
                <div class="card">
                    <h5 class="card-header">Delete Account</h5>
                    <div class="card-body">
                        <div class="mb-3 col-12 mb-0">
                            <div class="alert alert-warning">
                                <h6 class="alert-heading fw-bold mb-1 text-warning">Are you sure you want to delete your
                                    account?</h6>
                                <p class="mb-0 text-warning ">Once you delete your account, there is no going back. Please
                                    be certain.
                                </p>
                            </div>
                        </div>
                        <form id="formAccountDeactivation" action="{{ route('accountSetting.delete') }}" method="post">
                            @csrf
                            @method('delete')
                            <div class="form-check mb-3">
                                <input class="form-check-input {{ $errors->has('phone_number') ? ' is-invalid' : '' }}"
                                    type="checkbox" name="confirmed" value="1" id="confirmed" />
                                <label class="form-check-label" for="confirmed">I confirm my account
                                    deactivation</label>
                                @if ($errors->has('confirmed'))
                                    <span class="invalid-feedback text-right" style="display: block;" role="alert">
                                        <strong>{{ $errors->first('confirmed') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <button type="submit"
                                class="btn btn-danger deactivate-account btn-style-without-float">Deactivate
                                Account</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
