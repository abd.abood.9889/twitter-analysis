@extends('layouts.app')
@section('style')
@endsection
@section('content')

<style>
  .user_name{
    font: normal normal 600 37px/27px Segoe UI;
    color: rgb(130, 121, 121)
  }
  .details{
    font: normal normal 600 20px/20px Segoe UI;
    line-height: 40px;
  }
  .sec{
    
    background-color: rgb(252, 254, 254);
    padding: 40px;
    border-top: solid rgb(215, 209, 209) 1px;
  }
</style>
<section class="container mt-5">
  <h1 class="user_name" > <i class="fa fa-user-circle" aria-hidden="true"></i>
    {{$result->username}} </h1>
  <h4 class="details mt-3">{{$result->description}}</h4>

  <div class="row mt-4">
    <div class="col-md-6">
        <h4 class="details"><i class="fa fa-user" aria-hidden="true"></i> Name : {{$result->name}}</h4>
        
        <h4 class="details"> <i class="fa fa-check" aria-hidden="true"></i>  Is Verified 
          @if ($result->is_verified == 0)
          : <span class="badge me-1 bg-label-danger"> Not Verified</span>
          @else
          : <span class="badge me-1 bg-label-success"> Verified</span> 
          @endif
        </h4>
      <h4 class="details"><i class="fa fa-location-arrow" aria-hidden="true"></i> Location  : {{$result->location}}</h4>

    </div>

    <div class="col-md-6">
      <h4 class="details"> <i class="fa fa-link" aria-hidden="true"></i> URL  : {{$result->url}}</h4>
      <h4 class="details"> <i class="fa fa-plus" aria-hidden="true"></i> Following  : {{$result->following}}</h4>
      <h4 class="details"> <i class="fa fa-address-book" aria-hidden="true"></i> Followers : {{$result->followers}}</h4>

    </div>
    <br>
  </div>

  
</section>

<section class="container-fluid mt-5 sec">
  <div class="container">
    <h1 class="user_name" style="color:#0088be;">Latest Tweets  <i class="fa-brands fa-twitter" ></i> </h1>
    @if ($result->tweets->count())
    @foreach ($result->tweets as $item)
      <div class="row mt-4">
        <div class="col-md-6">
          <h4 class="details">{{$item->name}}</h4>
          <h4 class="details"> {{$item->content}}</h4>  
        </div>
    
        <div class="col-md-6">
          <h4 class="details"><i class="fa fa-thumbs-up" aria-hidden="true" ></i> Likes  : {{$item->likes}}</h4>
          <h4 class="details"> <i class="fa-brands fa-twitter" ></i> Retweets : {{$item->retweets}}</h4>
          <h4 class="details"><i class="fa-solid fa-reply"></i> Replies  : {{$item->replies}}</h4>
        </div>
        <br>
        <hr class="mt-4">
      </div>
    @endforeach
    @else
    <h4 class="details">No Tweets Found</h4>
    @endif
    
    
  </div>
 
</section>

<section class="container mt-5">
  <h1 class="user_name"> Write Your Feedack</i>
  <div class="row mt-4">
    <div class="col-md-12">
      <form method="POST" action="{{ route('feedbacks.store') }}">
        @csrf
        <input type="hidden" name="searche_id" value="{{$result->id}}">
        <div class="mb-3">

            <textarea id="feed" type="text" class="form-control @error('feed') is-invalid @enderror inp"
                name="feed"  required>{{ old('feed') }}</textarea>
            @error('feed')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
        <div class="row mb-0">
            <div class="col-md-8">
                <button type="submit" class="btn btn-primary"
                    style="background: #0088b; color:white;">
                    {{ __('submit') }}
                </button>
            </div>
        </div>
    </form>    </div>

    <br>
  </div>

  
</section>
@endsection
