@foreach ($searchs as $search)
    <tr>
        <td>{{ $search->id }}</td>
        <td>{{ $search->name}}</td>
        <td>{{ $search->username}}</td>
        
        <td>{{ $search->location }}</td>
        <td>{{ $search->followers }}</td>
        @if ($search->is_verified == true)
        <td> <span class="badge me-1 bg-label-success"> Verified</span></td>
        @else
        <td> <span class="badge me-1 bg-label-danger"> Not Verified</span></td>
        @endif
        <td>
            <div class="dropdown">
                <button type="button" class="btn p-0 dropdown-toggle hide-arrow" data-bs-toggle="dropdown">
                    <i class="bx bx-dots-vertical-rounded"></i>
                </button>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="{{ route('searsh.show', $search->username) }}"><i
                            class="bx bx-edit-alt me-1"></i>
                        Show </a>
                    <a class="dropdown-item deleteItem" data-url='{{ route('admin.searchs.destroy', $search->id) }}'
                        href="#"><i class="bx bx-trash me-1"></i>
                        Delete</a>
                </div>
            </div>
        </td>
    </tr>
@endforeach
