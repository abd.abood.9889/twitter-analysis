<?php

use App\Constants;
use App\Http\Controllers\Admin\FeedbackController;
use App\Http\Controllers\Admin\SearchController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\DashboardController;
use Illuminate\Support\Facades\Route;
use SebastianBergmann\CodeCoverage\Report\Html\Dashboard;

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');




Route::get('/login', [AuthController::class, 'index'])->name('login.index');
Route::post('/login', [AuthController::class, 'login'])->name('login');

Route::get('/register', [AuthController::class, 'register'])->name('register');
Route::post('/register', [AuthController::class, 'userRegister'])->name('userRegister');

Route::get('result/{username}', [SearchController::class, 'show' ])->name('searsh.show');
Route::post('search', [SearchController::class, 'search'])->name('search');

Route::post('feedbacks/store', [FeedbackController::class, 'store'])->name('feedbacks.store');

Route::group(['middleware' => ['auth']], function () {
    Route::post('/logout', [AuthController::class, 'logout'])->name('logout');

    //Admin
    Route::group(['middleware' => ['auth', 'roleAbilitiesMiddleware:' . Constants::Admin], 'prefix' => 'admin', 'as' => 'admin.'], function () {
        Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard.index');
        Route::resource('/searchs', SearchController::class )->except('show');
        Route::resource('/feedbacks', FeedbackController::class )->except('store');
        Route::resource('/users', UserController::class );
        Route::put('/Apikey-Update/{id}', [DashboardController::class, 'updateApiKey' ])->name('apikey.update');
    });
    
    //User
    Route::group(['middleware' => ['auth', 'roleAbilitiesMiddleware:' . Constants::User], 'prefix' => 'user', 'as' => 'user.'], function () {
    });
});